/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;
import View.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nikos
 */
public class Controller {

    public static Player p1 = new Player("Player 1");
    public static Player p2 = new Player("Player 2");

    int dicevalue = 0;
    //public  Player p1;
    // public Player p2;
    public Dice dice1, dice2;

    public Board board;
    public Jackpot jackpot;
    public JackWindow jack;
    public LotteryWindow lot;
    public RadioWindow rdio;
    public SundayWindow sunday;
    public forTurn turn;

    public MNeighbor neighborCard;
    //public GetMail mailwindow;
    public boolean lastGame = false, continueGame = false;
    public static int rounds;
    public static int round_count = 0;
    public static int nextmailcard = -1;
    public static int nextdealcard = -1;

    public Controller(Board b) {//Arxikopoihsh tou paixnidiou

        this.dice1 = new Dice();
        this.dice2 = new Dice();
        this.board = b;
        this.p1.currentPosition = -1;
        this.p2.currentPosition = -1;

        this.jackpot = new Jackpot();

        
    }

    public Controller(Board b, int games) {//Arxikopoihsh tou paixnidiou

        this.rounds = games;

        this.dice1 = new Dice();
        this.dice2 = new Dice();
        this.board = b;
        this.p1.currentPosition = -1;
        this.p2.currentPosition = -1;

        this.jackpot = new Jackpot();

        
        this.round_count++;
    }

    /**
     * Construtor.Intializes a new game.Creates a new board,2 players,deck for
     * mailcards and dealcards,collections of each player's owned cards.
     *
     */
    public void move() {
        board.Update();

        if (board.pl1.canPlay() == false) {
            board.pl1.setTurn(false);
            board.pl2.setTurn(true);
        }
        if (board.pl2.canPlay() == false) {
            board.pl2.setTurn(false);
            board.pl1.setTurn(true);
        }

        if (this.board.pl1.getTurn() == true) {
            checkJackpot(board.mainDice);
            this.board.pl1.setPosition(this.board.mainDice.getValue());
            this.board.panels.get(this.board.pl1.getPosition()).add(this.board.player1);
            this.board.money1.setText("Money:" + this.board.pl1.getMoney());

            board.canRollm = false;
            this.useEffect(board.board[board.pl1.getPosition()]);
            if (isSunday(board.pl1)) {
                sunday = new SundayWindow(board.pl1);

                board.Update();
            }

        } else if (this.board.pl2.getTurn() == true) {
            checkJackpot(board.mainDice);
            this.board.pl2.setPosition(this.board.mainDice.getValue());
            this.board.panels.get(this.board.pl2.getPosition()).add(this.board.player2);

            this.board.money2.setText("Money:" + this.board.pl2.getMoney());

            board.canRollm = false;
            this.useEffect(board.board[board.pl2.getPosition()]);
            board.Update();
            if (isSunday(board.pl2)) {
                sunday = new SundayWindow(board.pl2);
            }

        }
       

        board.Update();

    }

    public void useEffect(Position p) {
        if (p instanceof PayDay) {
            if (board.pl1.getTurn() == true) {
                board.pl1.setMoney(2500);
                board.Update();
                //pay bills
                if (board.pl1.getBills() <= board.pl1.getMoney()) {
                    board.pl1.setMoney(-board.pl1.getBills());
                    board.pl1.setBills(0);
                    board.Update();

                }
                if (board.pl1.getBills() > board.pl1.getMoney()) {
                    board.pl1.setBills(-board.pl1.getMoney());
                    board.pl1.setMoney(-board.pl1.getMoney());
                    board.Update();

                }
                //pay loans
                if (board.pl1.getLoans() != 0 && round_count < rounds) {
                    if (board.pl1.getMoney() >= board.pl1.getLoans() / 10) {
                        board.pl1.setLoans(-(board.pl1.getLoans() / 10));
                        board.pl1.setMoney(-(board.pl1.getLoans() / 10));
                       
                        board.Update();
                    } else {
                        loanWindow gameloan = new loanWindow(board.pl1);
                        board.pl1.setLoans(-(board.pl1.getLoans() / 10));
                        board.pl1.setMoney(-board.pl1.getMoney());
                        

                        board.Update();

                    }
                }
                if (board.pl1.getLoans() != 0 && round_count == rounds) {
                    if (board.pl1.getMoney() >= board.pl1.getLoans()) {
                        board.pl1.setMoney(-board.pl1.getLoans());
                        board.pl1.setLoans(-board.pl1.getLoans());
                       
                    } else {

                        board.pl1.setLoans(board.pl1.getMoney());
                        board.pl1.setMoney(-board.pl1.getMoney());
                        

                    }

                }
            }
            if (board.pl2.getTurn() == true) {
                board.pl2.setMoney(2500);
                board.Update();
                //pay bills
                if (board.pl2.getBills() <= board.pl2.getMoney()) {
                    board.pl2.setMoney(-board.pl2.getBills());
                    board.pl2.setBills(0);
                    board.Update();
                }
                if (board.pl2.getBills() > board.pl2.getMoney()) {
                    board.pl2.setBills(-board.pl2.getMoney());
                    board.pl2.setMoney(-board.pl2.getMoney());
                    
                    board.Update();

                }
                //pay loans
                if (board.pl2.getLoans() != 0 && round_count < rounds) {
                    if (board.pl2.getMoney() >= board.pl2.getLoans() / 10) {
                        board.pl2.setLoans(-(board.pl2.getLoans() / 10));
                        board.pl2.setMoney(-(board.pl2.getLoans() / 10));
                        
                        board.Update();
                    } else {
                        loanWindow gameloan = new loanWindow(board.pl2);
                        board.pl2.setLoans(-board.pl2.getLoans() / 10);
                        board.pl2.setMoney(-board.pl2.getMoney());
                        
                        board.Update();

                    }
                }
                if (board.pl2.getLoans() != 0 && round_count == rounds) {
                    if (board.pl2.getMoney() >= board.pl2.getLoans()) {
                        board.pl2.setMoney(-board.pl2.getLoans());
                        board.pl2.setLoans(-board.pl2.getLoans());

                    } else {

                        board.pl2.setLoans(board.pl2.getMoney());
                        board.pl2.setMoney(-board.pl2.getMoney());

                    }
                }

            }
            board.Update();
            if (rounds == round_count) {
                checkWinner(board.pl1, board.pl2);
            }
            board.roundDone = true;
            board.Update();

        }

        if (p instanceof Buyer) {

            if (board.pl1.getTurn() == true && board.pl1.getDeck().size() > 0) {
                playerDeck window = new playerDeck(board.pl1);
                
            }
            if (board.pl2.getTurn() == true && board.pl2.getDeck().size() > 0) {
                playerDeck window = new playerDeck(board.pl2);
                
            }

            board.Update();
            board.roundDone = true;
            board.info_name.setText("Sell an item");

        }
        if (p instanceof CasinoNight) {
            if (board.mainDice.getValue() % 2 == 0) {
                if (board.pl1.getTurn() == true) {
                    board.pl1.setMoney(500);
                    board.money1.setText("Money " + board.pl1.getMoney());
                    board.info_name.setText("Bank awards you with 500 euros");
                }
                if (board.pl2.getTurn() == true) {
                    board.pl2.setMoney(500);
                    board.money2.setText("Money " + board.pl2.getMoney());
                    board.info_name.setText("Bank awards you with 500 euros");
                }
            } else {
                if (board.pl1.getTurn() == true) {
                    board.pl1.setMoney(-500);
                    board.money1.setText("Money " + board.pl1.getMoney());
                    board.info_name.setText("You paid 500 euros.So unlucky.");
                    jackpot.setValue(500);
                }
                if (board.pl2.getTurn() == true) {
                    board.pl2.setMoney(-500);
                    board.money2.setText("Money " + board.pl2.getMoney());
                    board.info_name.setText("You paid 500 euros.So unlucky.");
                    jackpot.setValue(500);
                }
            }
            board.Update();
            board.roundDone = true;

        }
        if (p instanceof Loterry) {

            if (board.pl1.getTurn() == true) {
                lot = new LotteryWindow(board.pl1, board.pl2);

            }
            if (board.pl2.getTurn() == true) {
                lot = new LotteryWindow(board.pl2, board.pl1);

            }
            if (board.pl2.getTurn() == true) {
                board.info_name.setText("Both players roll the dice");

            }

            board.Update();
            board.roundDone = true;
        }

        if (p instanceof Mail1) {
            int next = drawNextMail();

            GetMail mailwin = new GetMail(board.deck1.getCard(next));
            if (board.pl1.getTurn() == true) {
                switch (board.deck1.getCard(next).getTypeEn()) {
                    case "Bill":
                        board.pl1.setBills(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros added to your bills");
                        break;
                    case "Charity":
                        jackpot.setValue(board.deck1.getCard(next).getEuro());
                        board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid for charity");
                        break;
                    case "PayTheNeighbor":
                        board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                        board.pl2.setMoney(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid to your neighbor");
                        break;
                    case "MadMoney":
                        board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                        board.pl1.setMoney(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid from your neighbor");
                        break;
                    case "MoveToDealBuyer":
                        goBuyer(board.pl1);
                        break;

                }
            }
            if (board.pl2.getTurn() == true) {
                switch (board.deck1.getCard(next).getTypeEn()) {
                    case "Bill":
                        board.pl2.setBills(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros added to your bills");
                        break;
                    case "Charity":
                        jackpot.setValue(board.deck1.getCard(next).getEuro());
                        board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid for charity");
                        break;
                    case "PayTheNeighbor":
                        board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                        board.pl1.setMoney(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid to your neighbor");
                        break;
                    case "MadMoney":
                        board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                        board.pl2.setMoney(board.deck1.getCard(next).getEuro());
                        board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid from your neighbor");
                        break;
                    case "MoveToDealBuyer":
                        goBuyer(board.pl2);
                        break;

                }
            }

            board.Update();
            board.roundDone = true;
        }
        if (p instanceof Mail2) {

            for (int i = 0; i < 2; i++) {
                int next = drawNextMail();

                GetMail mailwin = new GetMail(board.deck1.getCard(next));
                if (board.pl1.getTurn() == true) {
                    switch (board.deck1.getCard(next).getTypeEn()) {
                        case "Bill":
                            board.pl1.setBills(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros added to your bills");
                            break;
                        case "Charity":
                            jackpot.setValue(board.deck1.getCard(next).getEuro());
                            board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid for charity");
                            break;
                        case "PayTheNeighbor":
                            board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                            board.pl2.setMoney(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid to your neighbor");
                            break;
                        case "MadMoney":
                            board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                            board.pl1.setMoney(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid from your neighbor");
                            break;
                        case "MoveToDealBuyer":
                            goBuyer(board.pl1);
                            break;

                    }
                }
                if (board.pl2.getTurn() == true) {
                    switch (board.deck1.getCard(next).getTypeEn()) {
                        case "Bill":
                            board.pl2.setBills(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros added to your bills");
                            break;
                        case "Charity":
                            jackpot.setValue(board.deck1.getCard(next).getEuro());
                            board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid for charity");
                            break;
                        case "PayTheNeighbor":
                            board.pl2.setMoney(-board.deck1.getCard(next).getEuro());
                            board.pl1.setMoney(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid to your neighbor");
                            break;
                        case "MadMoney":
                            board.pl1.setMoney(-board.deck1.getCard(next).getEuro());
                            board.pl2.setMoney(board.deck1.getCard(next).getEuro());
                            board.info_name.setText(board.deck1.getCard(next).getEuro() + " euros paid from your neighbor");
                            break;
                        case "MoveToDealBuyer":
                            goBuyer(board.pl2);
                            break;

                    }
                }

            }

            board.Update();
            board.roundDone = true;

        }

        if (p instanceof Deal) {

            int next = drawNextDeal();

            if (board.pl1.getTurn() == true) {
                DealWin window = new DealWin(board.deck2.getCard(next), board.pl1);
            }
            if (board.pl2.getTurn() == true) {
                DealWin window = new DealWin(board.deck2.getCard(next), board.pl2);
            }

            board.Update();
            board.roundDone = true;

        }

        if (p instanceof Radio) {

            if (board.pl1.getTurn() == true) {
                board.info_name.setText("Both players roll the dice");
                rdio = new RadioWindow(board.pl1, board.pl2);

            }
            if (board.pl2.getTurn() == true) {
                board.info_name.setText("Both players roll the dice");
                rdio = new RadioWindow(board.pl2, board.pl1);
            }

            board.Update();
            board.roundDone = true;

        }

        if (p instanceof Sweepstake) {
            board.info_name.setText("Roll the dice!");
            board.canRolla = true;
            board.Update();

        }
        if (p instanceof YardSale) {
            board.info_name.setText("Roll the dice!");
            board.canRolla = true;

            board.Update();

        }

    }

    public void useAuxDice(Position p) {

        if (p instanceof Sweepstake) {

            if (board.pl1.getTurn() == true) {

                board.pl1.setMoney((board.auxDice.getValue() * 1000));
                board.money1.setText("Money:" + board.pl1.getMoney());
                board.info_name.setText("You rolled " + board.auxDice.getValue() + ".\n" + 1000 * board.auxDice.getValue() + "coins added to your bankroll.");

                board.canRolla = false;
                if (board.canRolla == false) {
                    board.roundDone = true;

                }
            }
            if (board.pl2.getTurn() == true) {

                board.pl2.setMoney((board.auxDice.getValue() * 1000));
                board.money2.setText("Money:" + board.pl2.getMoney());
                board.info_name.setText("You rolled " + board.auxDice.getValue() + ".\n" + 1000 * board.auxDice.getValue() + "coins added to your bankroll.");

                board.canRolla = false;
            }
            if (board.canRolla == false) {
                board.roundDone = true;

            }

        }
        if (p instanceof YardSale) {
            if (board.pl1.getTurn() == true) {
                board.pl1.setMoney(-(board.auxDice.getValue() * 100));
                board.money1.setText("Money:" + board.pl1.getMoney());
                board.info_name.setText("You rolled " + board.auxDice.getValue() + ".\n" + 100 * board.auxDice.getValue() + "coins removed from your bankroll.");
                board.canRolla = false;
                if (board.canRolla == false) {
                    board.roundDone = true;

                }
            }
            if (board.pl2.getTurn() == true) {
                board.pl2.setMoney(-(board.auxDice.getValue() * 100));
                board.money2.setText("Money:" + board.pl2.getMoney());
                board.info_name.setText("You rolled " + board.auxDice.getValue() + ".\n" + 100 * board.auxDice.getValue() + "coins removed from your bankroll.");

                board.canRolla = false;

            }
            if (board.canRolla == false) {
                board.roundDone = true;

            }
        }

    }

    /**
     * Accesor.Depending on what kind of position it is,the player does
     * something.
     */
    public boolean isSunday(Player p) {

        if (p.getPosition() == 6 || p.getPosition() == 13 || p.getPosition() == 20 || p.getPosition() == 27) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Transformer.Checks if the day is sunday or not(for footbalgame).
     */
    public void checkJackpot(Dice dice) {
        if (dice.getValue() == 6) {
            if (board.pl1.getTurn() == true) {
                jack = new JackWindow(jackpot);
                board.pl1.setMoney(jackpot.getValue());
                board.money1.setText("Money:" + board.pl1.getMoney());
                jackpot.setNull();

            }
            if (board.pl2.getTurn() == true) {
                jack = new JackWindow(jackpot);
                board.pl2.setMoney(jackpot.getValue());
                board.money2.setText("Money:" + board.pl2.getMoney());
                jackpot.setNull();
                board.jackl.setText("Current jack pot money:" + jackpot.getValue() + "euros");
            }

        }
    }

    /**
     * Transformer.Checks if the player can take the jackpot.
     */
    /**
     * Accesor.Moves the player arround the board.
     *
     */
    public void checkWinner(Player p1, Player p2) {
        int money1, money2;
        money1 = p1.getMoney() - p1.getLoans() - p1.getBills();
        money2 = p2.getMoney() - p2.getLoans() - p2.getBills();
        if (board.pl1.getPosition() == 30 && board.pl2.getPosition() == 30) {
            if (board.pl1.getMoney() > board.pl2.getMoney()) {
                WinnerWindow win = new WinnerWindow(p1.getName());
                System.out.println("Player 1 wins");
            } else {
                WinnerWindow win = new WinnerWindow(p2.getName());
                System.out.println("Player 2 wins");
            }
        }

    }

    public Player getPlayer1() {
        return this.p1;
    }

    public Player getPlayer2() {
        return this.p2;
    }

    public Dice getDice1() {

        return this.dice1;
    }

    public Dice getDice2() {

        return this.dice2;
    }

    public static boolean newGame() {
        if (p1.getPosition() == 30 && p2.getPosition() == 30) {
            return true;
        } else {
            return false;
        }
    }

    public int drawNextDeal() {
        nextdealcard++;

        return nextdealcard;

    }

    public int drawNextMail() {
        nextmailcard++;

        return nextmailcard;

    }

    public void goBuyer(Player p) {

        if (board.pl1.getTurn() == true) {
            for (int i = p.getPosition(); i < 30; i++) {
                if (board.board[i] instanceof Buyer) {
                    p.forcePosition(i);

                    board.panels.get(i).add(board.player1);
                    System.out.println(p.getPosition());
                    break;
                }
            }
        }
        if (board.pl2.getTurn() == true) {
            for (int i = p.getPosition(); i < 30; i++) {
                if (board.board[i] instanceof Buyer) {
                    p.forcePosition(i);
                    board.panels.get(i).add(board.player2);
                    System.out.println(p.getPosition());
                    break;
                }
            }
        }
    }

}
