/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Nikos
 */
public class YardSale extends Position {

    private static int timesPlaced = 0;

    public YardSale() {
        this.path = "resources/images/yard.png";
        timesPlaced++;
    }
    public String getPath() {
        return this.path;

    }

    public boolean canPlace() {
        if (timesPlaced <= 2) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setboardPosition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Accesor.Sets the position on the board.
     *
     *
     */

    @Override
    public int getboardPosition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Transformer.Return the position on the board Postcondition:A
     * boardposition has been set.
     *
     * @return
     */
    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Return a position's name.
     *
     */

    @Override
    public String getMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns a position's message.
     *
     */

    @Override
    public String getEffect() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * returns a position's window message.
     */

}
