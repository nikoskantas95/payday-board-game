/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.*;

/**
 *
 * @author Nikos
 */
public class Player {

    private String name;
    private ArrayList<DealCard> dealDeck;
    private ArrayList <String>dealInfo;
    public int currentPosition;
    private int money;
    private int loans;
    private int bills;
    private boolean plays = false;
    

    public Player(String name) {
        this.name = name;
        this.money = 2500;
        this.bills =0;
        this.loans = 0;
        this.currentPosition = -1;
        this.dealDeck = new ArrayList();
        this.dealInfo = new ArrayList();
    }

    public void setMoney(int mon) {
        this.money = this.money + mon;
        

    }

    /**
     * Accesor.Changes the amount of money a player has.
     */
    public void setBills(int bill) {
        if (bill != 0) {
            this.bills = this.bills + bill;
        } else {
            this.bills = 0;
        }

    }

    /**
     * Accesor.Configures a player's bills.
     */
    public void setLoans(int loan) {
        this.loans = this.loans + loan;

    }

    /**
     * Accesor.Configures a player's loans.
     */
    public void setPosition(int pos) {
        if (this.currentPosition + pos < 30) {
            this.currentPosition = this.currentPosition + pos;
        } else {
            this.currentPosition = 30;
        }

    }

    /**
     * Accesor.Changes the player's position on the board. Postcondition:The
     * player isnt on payday position.
     *
     */
    public int getMoney() {
        return this.money;

    }

    /**
     * Transformer.Returns the amount of money the player has.
     *
     */
    public int getBills() {
        return this.bills;
    }

    /**
     * Transformer.Returns the amount of bill the player has.
     *
     */
    public int getLoans() {
        return this.loans;
    }

    /**
     * Transformer.Returns the amount of loans the player owns.
     *
     */
    public int getPosition() {
        return this.currentPosition;
    }

    /**
     * Transformer.Returns the player's current position.
     */
    public void setTurn(boolean flag) {
        this.plays = flag;

    }

    /**
     * Accesor.Sets when the player plays.
     *
     * @return
     */
    public boolean getTurn() {
        return this.plays;//true if player plays else false
    }

    public boolean canPlay() {
        if (currentPosition != 30) {
            return true;
        } else {
            return false;//Checks if player has reached to the end of the month or not
        }
    }

    public String getName() {
        return this.name;
    }
    public void forcePosition(int pos){
        this.currentPosition=pos;
    }

    public void addCard(DealCard card) {
        dealDeck.add(card);
    }

    public ArrayList getDealInfo() {
        for (int i = 0; i < dealDeck.size(); i++) {
            String s=dealDeck.get(i).getMessage()+"--- sells for "+dealDeck.get(i).getValue()+" euros";
            dealInfo.add(i,s);
            
           

        }
        return dealInfo;

    }
    public ArrayList getDeck(){
        return this.dealDeck;
    }
    public DealCard getCard(int index){
        
        return dealDeck.get(index);
        
    }

}
