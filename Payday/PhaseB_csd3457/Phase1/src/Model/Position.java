/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Nikos
 */
public abstract class Position {
    protected int boardPosition;
    protected String name;
    protected String message;
    protected String effect;
    public String path;
    
    
    
    abstract public void setboardPosition();
    /**
     * Accesor.Sets the position on the board.
     * 
     * 
     */
    abstract public boolean canPlace();
    
    abstract public int getboardPosition();
    /**
     * Transformer.Return the position on the board
     * Postcondition:A boardposition has been set.
     * @return 
     */
    abstract public String getName();
    /**
     * Return a position's name.
     *  
     */
    abstract public String getMessage();
    /**
     * Returns a position's message.
     * 
     */
    abstract public String getEffect();
    /**
     * returns a position's window message.
     * 
     */
    abstract public String getPath();
    
    
    
}
