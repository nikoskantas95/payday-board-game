/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Nikos
 */
public abstract class Card {
    private String message;
    private String name;
    public String getName(){
        return this.name;
    }
    /**
     * returns the name of a card
     * Postcondition:A name has been set
     * 
     */
    public String getMessage(){
        return this.message;
    }
    /**
     * returns the message of a card
     * Postcondition:A message has been set
     */
   
    
}
