/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.Controller;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nikos
 */
public class DealDeck {

    private DealCard deck[] = new DealCard[20];
    private DealCard a;

    /**
     * Constructor.Creates a new deck of DealCards.
     */
    public DealDeck() {
        for (int i = 0; i < 20; i++) {
            try {
                deck[i] = new DealCard(i);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public DealCard getCard(int i) {

        return deck[i];
    }

    public void shuffleDeal() {

        Random r = new Random();

        for (int i = deck.length - 1; i > 0; i--) {
            int aux = r.nextInt(19);

            // Simple swap
            a = deck[aux];

            deck[aux] = deck[i];

            deck[i] = a;

        }

    }
}
