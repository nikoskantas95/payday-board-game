/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Nikos
 */
public class DealCard  {
     String[][] dealCards = new String[20][8];
    protected int cost;
    protected int value;
    protected String type;
    protected String TypeEn;
    protected String Message;
    protected String choise1;
    protected String choise2;
    protected String path;

    public DealCard(int quant) throws FileNotFoundException, IOException {

        String b[][] = new String[20][8];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/dealcards_greeklish.csv"));
        String line = br.readLine();
        int i = 0;
        while ((line = br.readLine()) != null) {
            b[i++] = line.split(splitBy);

        }
        br.close();

        this.type = b[0][0];
        this.TypeEn = b[0][1];
        this.Message = b[0 + quant][2];
        this.choise1 = b[0 + quant][6];
        this.choise2 = b[0+ quant][7];
        this.path = "resources/images/" + b[0 + quant][5];
        this.cost = Integer.parseInt(b[0 + quant][3]);
        this.value = Integer.parseInt(b[0 + quant][4]);
       

    }
    public String getChoise1(){
        
        return this.choise1;
        
    }
    /**
     * TRansformer.Returns Player's choise1
     * Precondition:Choise 1 is valid
     * @return 
     */
    public String getChoise2(){
        return this.choise2;
    }
    /**
     * TRansformer.Returns Player's choise2
     * Precondition:Choise 2 is valid
     * @return 
     */
    
public String getPath() {
        return this.path;
    }

    public int getCost() {
        return this.cost;
    }

    /**
     * Returns an int Postcondition:Depenting on type it may return null
     *
     * @return
     */
    public String getType() {
        return this.type;
    }

    /**
     * Returns the type of the Mail.
     *
     * @return
     */
    
  
    public String getMessage() {
        return this.Message;
    }
  
    public String getTypeEn() {
        return this.TypeEn;
    }

    /**
     * Returns the window message.

    /**
     * Returns the window message.
     */
    public int getValue(){
        return this.value;
    }
}


