/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nikos
 */
public class MNeighbor extends MailCard  {
    

    public MNeighbor(int quant)throws FileNotFoundException, IOException{
        

        String b[][]=new String[48][4];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/mailCards_greeklish.csv"));
        String line = br.readLine();
        int i=0;
        while((line = br.readLine()) !=null){
             b[i++] = line.split(splitBy);
            
        }
        br.close();
        

        this.type = b[32+quant][0];
        this.TypeEn = b[32+quant][1];
        this.Message = b[32+quant][2];
        this.choise = b[32+quant][3];
        this.path = "resources/images/"+b[32+quant][5];
        this.euro = Integer.parseInt(b[32+quant][4]);     
        this.quantity = 8;
       
        
      
    }
    

    public String getPath() {
        return this.path;
    }

    public int getEuro() {
        return this.euro;
    }

    /**
     * Returns an int Postcondition:Depenting on type it may return null
     *
     * @return
     */
    public String getType() {
        return this.type;
    }

    /**
     * Returns the type of the Mail.
     *
     * @return
     */
    public String getChoise() {
        return this.choise;
    }

    /**
     * Returns the window message.
     */
@Override
    public String getMessage() {
        return this.Message;
    }@Override
    public String getTypeEn() {
        return this.TypeEn;
    }
}

