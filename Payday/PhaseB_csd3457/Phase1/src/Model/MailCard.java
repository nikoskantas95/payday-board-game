/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nikos
 */
public abstract class MailCard {

    protected ClassLoader cldr;
    protected int quantity;
    //int mailCardCount = 0, dealCardCount = 0;
    String[][] mailCards = new String[48][4];
    String[][] dealCards = new String[20][8];
    protected int euro;
    protected String type;
    protected String TypeEn;
    protected String Message;
    protected String choise;
    protected String path;

    abstract public String getPath();

    abstract public int getEuro();

    /**
     * Returns an int Postcondition:Depenting on type it may return null
     *
     * @return
     */
    abstract public String getType();
    abstract public String getTypeEn();

    /**
     * Returns the type of the Mail.
     *
     * @return
     */
    abstract public String getChoise();

    abstract public String getMessage();

    /**
     * Returns the window message.
     */
}
