/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;

import java.util.*;

/**
 *
 * @author Nikos
 */
public class Dice {
    private int value;
   public Dice(){
        this.value=0;
    }
    public void rollDice(){
        Random r=new Random();
        int aux = r.nextInt(6)+1;
        this.value=aux;
        
    }
    /**
     * Accesor.Sets a random number between one and six to the dice's value.
     * 
     */
    public int getValue(){
        
        return this.value;
    }
   
    
    /**
     * Transformer.Returns the dice's current value.
     */
     public void setValue(int val){
        this.value=val;
    }
}
