/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.Controller;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nikos
 */
public class MailDeck {

    private MailCard deck[] = new MailCard[48];
    MailCard a;

    /**
     * Constructor.Creates a new deck of mail cards.
     */
    public MailDeck() {
        for (int i = 0; i < 8; i++) {
            try {
                deck[i] = new MNeighbor(i);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for (int i = 8; i < 16; i++) {
            try {
                deck[i] = new MadMoney(i - 8);
          
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 16; i < 24; i++) {
            try {
                deck[i] = new Charity(i - 16);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 24; i < 32; i++) {
            try {
                deck[i] = new Advertisment(i - 24);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 32; i < 40; i++) {
            try {
                deck[i] = new Bill(i - 32);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 40; i < 48; i++) {
            try {
                deck[i] = new MoveDeal(i - 40);

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public MailCard getCard(int i) {
       
        return deck[i];
    }

    public void shuffle() {

        Random r = new Random();

        for (int i = deck.length - 1; i > 0; i--) {
            int aux = r.nextInt(47);

            // Simple swap
            a = deck[aux];

            deck[aux] = deck[i];

            deck[i] = a;
             

        }

    }
}
