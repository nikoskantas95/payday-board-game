/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class loanWindow extends JFrame {

    public int dice1, dice2;
    public int choise1, choise2;

    public JPanel pan;
    public JButton b;
    public JLabel l;
    public JComboBox box;
    public String arr[] = {"1000", "2000", "3000", "4000", "5000", "6000", "7000", "8000", "9000", "10000"};
    public boolean complete = false;
    public Player p;

    public loanWindow(Player pl) {
        this.p=pl;
       
        this.l = new JLabel("Choose the amount of money you want to loan:");
        l.setFont(new Font("Arial", Font.PLAIN, 24));
        this.setSize(new Dimension(500, 500));
        this.pan = new JPanel();
        this.box = new JComboBox(arr);
        pan.add(l);
        pan.add(box);
        box.addActionListener(new list());
        this.add(pan);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }
    
     public class list implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(box == (JComboBox) e.getSource()){
                p.setMoney(Integer.parseInt((String)box.getSelectedItem()));
                p.setLoans(Integer.parseInt((String)box.getSelectedItem()));
                dispose();
            }
           
        }
        
    }
   

}
