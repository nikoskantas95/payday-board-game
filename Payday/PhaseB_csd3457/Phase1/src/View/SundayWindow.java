/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class SundayWindow extends JFrame {

    private JLabel lab;
    private JPanel pan;
    private JButton barc, real, tie, no;
    private Player player;
    private Dice d;

    public SundayWindow(Player p) {
        this.setSize(new Dimension(500, 300));
        barc = new JButton("Win for Barcelona");
        real = new JButton("Win for Real Madrid");
        tie = new JButton("Tie");
        no = new JButton("I don't want to bet");
        pan = new JPanel();
        d = new Dice();
        this.player = p;
        this.setTitle("Sunday's football game");
        ImageIcon aux = new ImageIcon(new ImageIcon("resources/images/Barcelona_Real.jpg").getImage().getScaledInstance(250, 125, Image.SCALE_DEFAULT));
       
        lab=new JLabel(aux);

        barc.addActionListener(new list());
        real.addActionListener(new list());
        tie.addActionListener(new list());
        no.addActionListener(new list());
        pan.add(lab);
        pan.add(barc);
        pan.add(real);
        pan.add(tie);
        pan.add(no);
        this.add(pan);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public class list implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean once=true;

            if (barc == (JButton) e.getSource() && once == true) {
                d.rollDice();
                if (d.getValue() == 1 || d.getValue() == 2) {
                    player.setMoney(500);
                } else {
                    player.setMoney(-500);
                }
                once = false;
                dispose();

            }
            if (real == (JButton) e.getSource() && once == true) {
                d.rollDice();
                if (d.getValue() == 3 || d.getValue() == 4) {
                    player.setMoney(500);

                } else {
                    player.setMoney(-500);
                }
                once = false;
                dispose();

            }
            if (tie == (JButton) e.getSource() && once == true) {
                d.rollDice();
                if (d.getValue() == 5 || d.getValue() == 6) {
                    player.setMoney(500);

                } else {
                    player.setMoney(-500);
                }
                once = false;
                dispose();

            }
            if (no == (JButton) e.getSource() && once == true) {
                dispose();
                once = false;

            }

        }

    }

}
