/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class DealWin extends JFrame {

    private JButton deal, ignore;
    private JPanel pan, auxpan;
    private JLabel labicon, labmess;
    private Player pl;
    private DealCard dc;

    public DealWin(DealCard card, Player p) {
        this.setSize(new Dimension(750, 500));
        this.dc = card;
        this.pl = p;
        pan = new JPanel();
        deal = new JButton("Take the deal");
        ignore = new JButton("Ignore the deal");
        ImageIcon aux = new ImageIcon(new ImageIcon(card.getPath()).getImage().getScaledInstance(400, 300, Image.SCALE_DEFAULT));
        labicon = new JLabel(aux);
        labmess = new JLabel();
        labmess.setText(card.getMessage());
        labmess.setFont(new Font("Arial Black", Font.PLAIN, 16));
        pan.setLayout(new BorderLayout());
        auxpan = new JPanel();
        auxpan.setLayout(new BorderLayout());
        auxpan.add(ignore, BorderLayout.EAST);
        auxpan.add(deal, BorderLayout.WEST);
        ignore.setPreferredSize(new Dimension(150, 75));
        deal.setPreferredSize(new Dimension(75, 150));
        this.add(pan);
        pan.add(labicon, BorderLayout.NORTH);
        pan.add(labmess, BorderLayout.CENTER);
        pan.add(auxpan, BorderLayout.SOUTH);
        deal.addActionListener(new list());
        ignore.addActionListener(new list());

        this.setVisible(true);

        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public class list implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (ignore == (JButton) e.getSource()) {
                dispose();

            }
            if (deal == (JButton) e.getSource() && pl.getMoney() > dc.getCost()) {
                pl.addCard(dc);
                pl.setMoney(-dc.getCost());
                dispose();

            }

        }

    }

}
