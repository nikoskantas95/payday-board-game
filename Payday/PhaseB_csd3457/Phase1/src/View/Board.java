/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class Board {

    public boolean complete = false;
    public Position board[] = new Position[31];
    public String day;
    public JFrame window;
    public backgroundPaint boardPanel, infoPanel, actionPanel, utilitybox, backaux2, dealCard, jackpot, mailCard;
    public JPanel infobox;
    public JLabel diconm, dicona, player1, player2, money1, money2, loans1, loans2, bills1, bills2, info_name, jackl, info_turn;
    public Controller control = new Controller(this);
    public JButton button, auxbutton, endturn1, endturn2, getloan1, getloan2, dealCards;
    public Player pl1, pl2;
    public int p1_position = 0, p2_position, simpleRollValue;
    public ArrayList<backgroundPaint> panels = new ArrayList<backgroundPaint>();
    public Dice mainDice, auxDice;
    public boolean roundDone = true, canRolla = false, canRollm = true, multipleloans = false;
    public MailDeck deck1;
    public DealDeck deck2;
    public forTurn turn;

    private ImageIcon image;
    private int casino = 0, buyer = 0, deal = 0, loterry = 0, mail1 = 0, mail2 = 0, radio = 0, sweepstake = 0, yard = 0;
    // private Dice dice=new Dice();

    public Board() {

        /**
         * Constructor Creates a new Board With random positions First position
         * is always the Start position,whereas the last position is always the
         * PayDay position
         */
        boolean flag = false;

        this.mainDice = control.dice1;
        this.auxDice = control.dice2;
        Image icon;
        this.pl1 = control.p1;
        this.pl2 = control.p2;
        this.deck1 = new MailDeck();
        this.deck2 = new DealDeck();
        deck1.shuffle();
        deck2.shuffleDeal();

        window = new JFrame("PayDay");
        window.setLayout(new BorderLayout());
        icon = new ImageIcon("resources/images/bg_green.png").getImage();
        Image background = new ImageIcon("resources/images/bg_green.png").getImage();
        boardPanel = new backgroundPaint(icon);
        boardPanel.setLayout(new GridLayout(0, 7));
        boardPanel.setPreferredSize(new Dimension(300, 300));
        infoPanel = new backgroundPaint(icon);
        infoPanel.setLayout(new BorderLayout());
        actionPanel = new backgroundPaint(icon);
        actionPanel.setLayout(new BorderLayout());
        infoPanel.setPreferredSize(new Dimension(300, 400));
        actionPanel.setPreferredSize(new Dimension(400, 400));
        icon = new ImageIcon("resources/images/start.png").getImage();

        backgroundPaint start = new backgroundPaint(icon);

        ImageIcon image_blue = new ImageIcon(new ImageIcon("resources/images/pawn_blue.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
        ImageIcon image_yellow = new ImageIcon(new ImageIcon("resources/images/pawn_yellow.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
        player1 = new JLabel(image_blue);
        player2 = new JLabel(image_yellow);
        start.setPreferredSize(new Dimension(200, 200));
        start.add(player1);
        start.add(player2);
        infoPanel.add(start, BorderLayout.NORTH);
        infobox = new JPanel();
        utilitybox = new backgroundPaint(background);
        utilitybox.setLayout(new BorderLayout());
        icon = new ImageIcon("resources/images/dealCard.png").getImage();
        dealCard = new backgroundPaint(icon);
        dealCard.setPreferredSize(new Dimension(150, 100));
        dealCard.setOpaque(true);
        utilitybox.add(dealCard, BorderLayout.NORTH);
        icon = new ImageIcon("resources/images/jackpot.png").getImage();
        jackpot = new backgroundPaint(icon);
        jackpot.setLayout(new BorderLayout());
        jackpot.setPreferredSize(new Dimension(250, 200));
        utilitybox.add(jackpot, BorderLayout.CENTER);
        icon = new ImageIcon("resources/images/mailCard.png").getImage();
        dealCards = new JButton("My Deal Cards");
        mailCard = new backgroundPaint(icon);
        mailCard.setPreferredSize(new Dimension(150, 100));
        dealCard.add(dealCards);
        utilitybox.add(mailCard, BorderLayout.SOUTH);
        ImageIcon mailcards = new ImageIcon("resources/images/mailCard.png");
        ImageIcon jack = new ImageIcon(new ImageIcon("resources/images/jackpot.png").getImage().getScaledInstance(253, 121, Image.SCALE_DEFAULT));
        jackl = new JLabel("Current jack pot money:" + control.jackpot.getValue() + "euros");
        jackl.setOpaque(false);
        jackl.setFont(new Font("Arial Black", Font.PLAIN, 16));
        jackpot.add(jackl, BorderLayout.SOUTH);
        infobox.setLayout(new GridLayout(0, 1));
        infobox.setPreferredSize(new Dimension(300, 300));
        info_name = new JLabel("");

        info_turn = new JLabel("Turn :");
        //JLabel info_effect = new JLabel("todo");
        infobox.add(info_name);
        // infobox.add(info_timeleft);
        infobox.add(info_turn);
        //infobox.add(info_effect);
        icon = new ImageIcon("resources/images/bg_green.png").getImage();
        infoPanel.add(infobox, BorderLayout.SOUTH);
        backaux2 = new backgroundPaint(icon);
        backaux2.setLayout(new GridLayout(2, 0));
        infoPanel.add(utilitybox, BorderLayout.CENTER);
        JPanel p1_info = new JPanel();
        p1_info.setLayout(new GridLayout(0, 1));
        p1_info.setPreferredSize(new Dimension(300, 300));
        JPanel p2_info = new JPanel();
        p2_info.setLayout(new GridLayout(0, 1));
        p2_info.setPreferredSize(new Dimension(300, 300));
        money1 = new JLabel("Money:" + pl1.getMoney());
        money1.setFont(new Font("Arial", Font.PLAIN, 24));
        loans1 = new JLabel("Loans:0");
        bills1 = new JLabel("Bills:0");
        getloan1 = new JButton("Get loan");
        loans1.setFont(new Font("Arial", Font.PLAIN, 24));
        bills1.setFont(new Font("Arial", Font.PLAIN, 24));
        info_turn.setFont(new Font("Arial", Font.PLAIN, 24));
        info_name.setFont(new Font("Arial", Font.PLAIN, 16));
        endturn1 = new JButton("End turn");
        money2 = new JLabel("Money:" + pl2.getMoney());
        loans2 = new JLabel("Loans:0");
        bills2 = new JLabel("Bills:");
        money2.setFont(new Font("Arial", Font.PLAIN, 24));
        loans2.setFont(new Font("Arial", Font.PLAIN, 24));
        bills2.setFont(new Font("Arial", Font.PLAIN, 24));
        getloan2 = new JButton("Get loan");
        endturn2 = new JButton("End turn");
        button = new JButton("Roll the dice!");
        button.setSize(300, 150);
        auxbutton = new JButton("Roll the dice!");
        auxbutton.setSize(300, 150);
        p1_info.add(money1);
        p1_info.add(loans1);
        p1_info.add(bills1);
        p1_info.add(endturn1);
        p1_info.add(getloan1);
        p2_info.add(money2);
        p2_info.add(loans2);
        p2_info.add(bills2);
        p2_info.add(endturn2);
        p2_info.add(getloan2);

        actionPanel.add(p1_info, BorderLayout.NORTH);
        actionPanel.add(backaux2, BorderLayout.CENTER);
        actionPanel.add(p2_info, BorderLayout.SOUTH);
        JLabel day;
        day = new JLabel(new ImageIcon("resources/images/monday.png"));
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/tuesday.png"));
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/wednesday.png"));
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/thursday.png"));
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/friday.png"));
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/saturday.png"));
        day.setOpaque(true);
        boardPanel.add(day);
        day = new JLabel(new ImageIcon("resources/images/sunday.png"));
        boardPanel.add(day);
        for (int i = 0; i < 30; i++) {
            backgroundPaint auxp;

            while (flag == false) {
                Random r = new Random();
                int aux = r.nextInt(9);
                if (aux == 0 && casino < 2) {
                    board[i] = new CasinoNight();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    casino++;
                    flag = true;
                }
                if (aux == 1 && buyer < 6) {

                    board[i] = new Buyer();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    buyer++;

                    flag = true;
                }
                if (aux == 2 && mail1 < 4) {
                    board[i] = new Mail1();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    mail1++;
                    flag = true;
                }
                if (aux == 3 && mail2 < 4) {
                    board[i] = new Mail2();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    mail2++;
                    flag = true;
                }
                if (aux == 4 && deal < 5) {
                    board[i] = new Deal();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    deal++;
                    flag = true;
                }
                if (aux == 5 && radio < 2) {
                    board[i] = new Radio();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    radio++;
                    flag = true;
                }
                if (aux == 6 && sweepstake < 2) {
                    board[i] = new Sweepstake();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    sweepstake++;
                    flag = true;
                }
                if (aux == 7 && yard < 2) {
                    board[i] = new YardSale();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);

                    boardPanel.add(panels.get(i));
                    yard++;
                    flag = true;
                }
                if (aux == 8 && loterry < 3) {
                    board[i] = new Loterry();
                    icon = new ImageIcon(board[i].getPath()).getImage();
                    auxp = new backgroundPaint(icon);
                    panels.add(auxp);
                    boardPanel.add(panels.get(i));
                    loterry++;
                    flag = true;
                }

            }
            flag = false;

        }
        board[30] = new PayDay();
        icon = new ImageIcon("resources/images/pay.png").getImage();

        backgroundPaint payday = new backgroundPaint(icon);
        panels.add(payday);
        boardPanel.add(payday);
        ImageIcon diceIcon = new ImageIcon("resources/images/dice.jpg");//stores the current dice's icon
        diconm = new JLabel();
        dicona = new JLabel();
        dicona.setIcon(diceIcon);
        diconm.setIcon(diceIcon);
        backaux2.add(diconm);
        backaux2.add(dicona);
        backaux2.add(button);
        backaux2.add(auxbutton);
        button.addActionListener(new listener());
        auxbutton.addActionListener(new listener());
        endturn1.addActionListener(new listener());
        endturn2.addActionListener(new listener());
        getloan1.addActionListener(new listener());
        getloan2.addActionListener(new listener());
        dealCards.addActionListener(new listener());
        window.add(boardPanel, BorderLayout.CENTER);
        window.add(infoPanel, BorderLayout.WEST);
        window.add(actionPanel, BorderLayout.EAST);
        window.setVisible(true);
         turn=new forTurn(this.pl1,this.pl2);
        Update();
        
        window.pack();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        /* 
        /
        Events
         */
    }

    public class listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (button == (JButton) e.getSource() && canRollm == true) {//simpleroll= o idios pextis 3anarixnei
                mainDice.rollDice();
                String aux = Integer.toString(mainDice.getValue());
                ImageIcon daux = new ImageIcon("resources/images/dice-" + aux + ".jpg");
                diconm.setIcon(daux);
                backaux2.repaint();
                actionPanel.repaint();
                window.repaint();              
                control.move();

               
            }
            if (auxbutton == (JButton) e.getSource() && canRolla == true) {//simpleroll= o idios pextis 3anarixnei

                auxDice.rollDice();
                String aux = Integer.toString(auxDice.getValue());
                ImageIcon daux = new ImageIcon("resources/images/dice-" + aux + ".jpg");
                dicona.setIcon(daux);
                if (pl1.getTurn() == true) {
                    control.useAuxDice(board[pl1.getPosition()]);
                }

                if (pl2.getTurn() == true) {
                    control.useAuxDice(board[pl2.getPosition()]);
                }

                backaux2.repaint();
                actionPanel.repaint();
                window.repaint();
                canRolla = false;

            }

            if (endturn1 == (JButton) e.getSource() && pl1.getTurn() == true && roundDone == true /*&& changeButton == true*/) {
                pl1.setTurn(false);
                pl2.setTurn(true);
                roundDone = false;
                canRollm = true;
                info_turn.setText("Current turn:Player 2");
                roundDone = false;
                multipleloans = false;
                canRollm = true;
                money1.setText("Money:" + pl1.getMoney());
                money2.setText("Money:" + pl2.getMoney());
                control.board.jackl.setText("Current jackpot money:" + control.jackpot.getValue() + "euros");

            }
            if (endturn2 == (JButton) e.getSource() && pl2.getTurn() == true && roundDone == true/* && changeButton == false*/) {
                info_turn.setText("Current turn:Player 1");
                Update();

                pl1.setTurn(true);
                pl2.setTurn(false);
                roundDone = false;
                canRollm = true;
                multipleloans = false;
                money1.setText("Money:" + pl1.getMoney());
                money2.setText("Money:" + pl2.getMoney());
                control.board.jackl.setText("Current jack pot money:" + control.jackpot.getValue() + "euros");

            }
            if (getloan1 == (JButton) e.getSource() && pl1.getTurn() == true && multipleloans == false) {
                loanWindow lowin1 = new loanWindow(pl1);

            }
            if (getloan2 == (JButton) e.getSource() && pl2.getTurn() == true && multipleloans == false) {
                loanWindow lowin2 = new loanWindow(pl2);

            }
            if (dealCards == (JButton) e.getSource()) {
                if (pl1.getTurn() == true) {
                    new PlayerDeckView(pl1);
                }
                if (pl2.getTurn() == true) {
                    new PlayerDeckView(pl2);
                }

            }

        }

    }

    public void Update() {
        money1.setText("Money:" + pl1.getMoney());
        money2.setText("Money:" + pl2.getMoney());
        loans1.setText("Loans:" + pl1.getLoans());
        loans2.setText("Loans:" + pl2.getLoans());
        bills1.setText("Bills:" + pl1.getBills());
        bills2.setText("Bills:" + pl2.getBills());
        control.board.jackl.setText("Current jackpot money:" + control.jackpot.getValue() + "euros");
        

    }

}
