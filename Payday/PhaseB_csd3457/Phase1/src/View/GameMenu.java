/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class GameMenu extends JFrame {

    private JPanel pan;
    private JLabel lab;
    private JButton newg, cont;
    private JComboBox box;
    private String arr[] = {"0","1", "2", "3"};
    private int gameValue;
    private Boolean start=false;

    public GameMenu() {
        this.setSize(new Dimension(500, 500));
        this.setTitle("PayDay");
        this.lab=new JLabel(new ImageIcon("resources/images/banner.jpg"));
        this.pan = new JPanel();
        this.newg = new JButton("New Game");
        this.cont = new JButton("Continue Game");       
        this.box = new JComboBox(arr);
        pan.add(newg);
        pan.add(lab);
        pan.add(box);        
        box.setVisible(false);
        pan.add(cont);
        newg.addActionListener(new butLis());
        cont.addActionListener(new butLis());
        box.addActionListener(new boxLis());        
        this.add(pan);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    public class butLis implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (newg == (JButton) e.getSource()) {
                box.setVisible(true);
                
            }
           
        }

    }

    public class boxLis implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (box == (JComboBox) e.getSource()) {
               gameValue= Integer.parseInt((String) box.getSelectedItem());
               start=true;
               dispose();
               
              
            }
        }

    }
    public int getGames(){
        return gameValue;
    }
    public boolean canStart(){
        return this.start;
    }
    
}
