/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class JackWindow extends JFrame {
    private JPanel panel;
    private JLabel label;
    public JackWindow(Jackpot jack){
        this.setSize(new Dimension(300,300));
        panel=new JPanel();
        label=new JLabel();
        label.setText("Congratulation you won "+jack.getValue()+" euros");
        panel.add(label);
        this.add(panel);
        this.setVisible(true);
        
    }
    
}
