/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class RadioWindow extends JFrame {

    public JPanel panel;
    public JButton b1, b2;
    public JLabel lab1, lab2;
    private Player first;
    private Player second;
    public Dice d1, d2;
    private boolean roll1 = true, roll2 = false;

    public RadioWindow(Player first, Player second) {

        b1 = new JButton();
        b2 = new JButton();
        d1 = new Dice();
        d2 = new Dice();
        this.first = first;
        this.second = second;
        this.setSize(new Dimension(500, 500));
        this.panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        this.lab1 = new JLabel(first.getName() + " roll the dice.");
        this.lab2 = new JLabel(second.getName() + " roll the dice.");
        b1.setIcon(new ImageIcon("resources/images/dice.jpg"));
        b2.setIcon(new ImageIcon("resources/images/dice.jpg"));
        panel.add(lab1);
        panel.add(b1);
        panel.add(lab2);
        panel.add(b2);
        b1.addActionListener(new list());
        b2.addActionListener(new list());
        this.add(panel);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

    }

    public class list implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (b1 == (JButton) e.getSource() && roll1 == true) {
                d1.rollDice();
                b1.setIcon(new ImageIcon("resources/images/dice-" + d1.getValue() + ".jpg"));
                roll1 = false;
                roll2 = true;
            }
            if (b2 == (JButton) e.getSource() && roll2 == true) {
                d2.rollDice();
                b2.setIcon(new ImageIcon("resources/images/dice-" + d2.getValue() + ".jpg"));
                if (d1.getValue() != d2.getValue()) {
                    roll2 = false;
                    if (d1.getValue() > d2.getValue()) {
                        first.setMoney(1000);

                    }

                    if (d2.getValue() > d1.getValue()) {
                        second.setMoney(1000);

                    }

                }
            }

        }

    }

}
