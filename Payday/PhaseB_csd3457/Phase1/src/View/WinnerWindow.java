/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class WinnerWindow extends JFrame {
    private JPanel pan;
    private JLabel lab,win;

    public WinnerWindow(String winner) {
        this.setSize(new Dimension(500,500));
        this.pan=new JPanel();
        pan.setLayout(new GridLayout(0,1));
        this.lab=new JLabel(new ImageIcon("resources/images/win.gif"));
        this.win=new JLabel(winner+" WINS!");
        win.setFont(new Font("Arial Black", Font.PLAIN, 36));
        pan.add(lab);
        pan.add(win);
        this.add(pan);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        

    }

}
