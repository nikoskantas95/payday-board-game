/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author Nikos
 */
public class LotteryWindow extends JFrame {

    public int dice1, dice2;
    public int choise1, choise2;
    
    public Dice d;
    public Dice d2;
    public JPanel p;
    public JButton b;
    public JButton b2;
    public JComboBox box1, box2;
    public String arr[] = {"1", "2", "3", "4", "5", "6"};
    public JLabel l1, l2;
    public boolean complete = false;
    public Player first, second;
    public boolean win1 = false, win2 = false;

    public LotteryWindow(Player pl1, Player pl2) {
        this.setSize(new Dimension(500, 500));
        this.first = pl1;
        this.second = pl2;
        p = new JPanel();
        b = new JButton();
        d=new Dice();
        box1 = new JComboBox(arr);
        box2 = new JComboBox(arr);
        
        b.setIcon(new ImageIcon("resources/images/dice.jpg"));
       
        p.add(b);
        l1 = new JLabel(pl1.getName() + " choose a number.");
        l2 = new JLabel(pl2.getName() + " choose a number.");
        p.add(l1);
        b.setVisible(false);
        box1.addActionListener(new listbox());
        box2.addActionListener(new listbox());
        p.add(box1);
        p.add(l2);
        p.add(box2);

        b.addActionListener(new listbutton());
        this.add(p);

        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public class listbutton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (b == (JButton) e.getSource() && complete == false) {
                d.rollDice();
                b.setIcon(new ImageIcon("resources/images/dice-" + d.getValue() + ".jpg"));
                if (choise1 == d.getValue()) {
                    win1 = true;
                    win2 = false;
                    complete = true;
                    first.setMoney(1000);
                    
                }
                if (choise2 == d.getValue()) {
                    win1 = false;
                    win2 = true;
                    complete = true;
                    second.setMoney(1000);
                   
                }
                

            }

        }
    }

    public class listbox implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (box1 == (JComboBox) e.getSource()) {
                choise1 = Integer.parseInt((String) box1.getSelectedItem());
            }
            if (box2 == (JComboBox) e.getSource()) {
                choise2 = Integer.parseInt((String) box2.getSelectedItem());
                b.setVisible(true);
            }

        }

    }
     public Player lotWin(){
            if(win1==true)
                return first;
            else if(win2==true)
                return second;
            else return null;
           
           
       
            
        }
}
