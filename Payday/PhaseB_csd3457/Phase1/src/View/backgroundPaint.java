/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;

public class backgroundPaint extends JPanel{
    Image image;

        public backgroundPaint(Image img) {
            image=img;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, this.getSize().width, this.getSize().height, this);
          
        }

}